#include "mbed.h"
#include "Mavericks485.h"

Mavericks485::Mavericks485(PinName TXpin, PinName RXpin, PinName REDEpin) :
devSerial(TXpin, RXpin),
//pc(PA_2, PA_3),
REDE(REDEpin)
{
    REDE = 0;
    error = 0;
    
}



unsigned char Mavericks485::calc_chksum (volatile unsigned char *Data, unsigned char datalength){
    unsigned int chksum = 0;
    for(unsigned char f=0; f<datalength; f++){
        chksum += Data[f];
    }
        
    return ((chksum%255)+1);
}


void Mavericks485::sort(){
    
    switch(receive){
      case 0xFA:
            switch (receive_state) {
                case 0:
                    receive_state = 1;
                    break;
                case 1:
                    receive_state = 0;
                    break;
                case 2: //only slave mode
                    if(address == 0xFA){
                        receive_state = 3;
                    }else{
                        receive_state = 0;
                    }
                    break;
                case 3:
                    dataSize = 0xFA;
                    receive_state = 4;
                    i = 0;
                    break;
                case 4:
                    data[i] = 0xFA;
                    if(i < (dataSize-1)){
                        i++;
                    }else{
                        receive_state = 5;
                    }
                    break;
                case 5:
                    if(calc_chksum(data, dataSize) == 0xFA){
                        datastate = true;
                        for(unsigned char f=0; f<dataSize; f++){
                            databuf[f] = data[f];
                        }
                        databuf[255] = dataSize;
                        dataSize = 0;
                    }else{
                        datastate = false;
                        dataSize = 0;
                    }
                    receive_state = 0;
                    break;
            }
            break;
      case 0xAF:
            switch (receive_state) {
                case 0:
                    break;
                case 1:
                    if(devMode == 0){
                        receive_state = 2;
                    }else{
                        receive_state = 3;
                    }
                    break;
                case 2: //only slave mode
                    if(address == 0xAF){
                        receive_state = 3;
                    }else{
                        receive_state = 0;
                    }
                    break;
                case 3:
                    dataSize = 0xAF;
                    receive_state = 4;
                    i = 0;
                    break;
                case 4:
                    data[i] = 0xAF;
                    if(i < (dataSize-1)){
                        i++;
                    }else{
                        receive_state = 5;
                    }
                    break;
                case 5:
                    if(calc_chksum(data, dataSize) == 0xAF){
                        datastate = true;
                        for(unsigned char f=0; f<dataSize; f++){
                            databuf[f] = data[f];
                        }
                        databuf[255] = dataSize;
                        dataSize = 0;
                    }else{
                        datastate = false;
                        dataSize = 0;
                    }
                    receive_state = 0;
                    break;
            }
            break;
        default:
            switch (receive_state) {
                case 0:
                    break;
                case 1:
                    receive_state = 0;
                    break;
                case 2: //only slave mode
                    if(receive == address){
                        receive_state = 3;
                    }else{
                        receive_state = 0;
                    }
                    break;
                case 3:
                    dataSize = receive;
                    receive_state = 4;
                    i = 0;
                    break;
                case 4:
                    data[i] = receive;
                    if(i < (dataSize-1)){
                        i++;
                    }else{
                        receive_state = 5;
                    }
                    break;
                case 5:
                    if(calc_chksum(data, dataSize) == receive){
                        datastate = true;
                        for(unsigned char f=0; f<dataSize; f++){
                            databuf[f] = data[f];
                        }
                        databuf[255] = dataSize;
                        dataSize = 0;
                    }else{
                        datastate = false;
                        dataSize = 0;
                    }
                    receive_state = 0;
                    break;
            }
            break;
    }
    
}


void Mavericks485::readattach (void){
    
//    pc.printf("c1\n");

    receive = devSerial.getc();
    sort();

//    pc.putc(receive);

}


void Mavericks485::begin(unsigned char myAddress, bool enable_attach, int baud){
    address = myAddress;
    enable = enable_attach;
    serialbaud = baud;

    devSerial.baud(serialbaud);
//    pc.baud(115200);
    
    sendtime = (float)((1.0/serialbaud)*10000000.0*5.0);
  
    if(address == 0x00){
        devMode = 1;
    }
  
    if(enable){
        devSerial.attach(mbed::Callback<void ()>(this, &Mavericks485::readattach), mbed::RawSerial::RxIrq);
    }

}


void Mavericks485::read(unsigned char *finalData, unsigned char datalength){

  if(enable == false){
      REDE = 0;
      if(devSerial.readable()) {
        receive = devSerial.getc();
        sort();
    }
  }
    
//    if(datastate){
        for(unsigned char f=0; f<databuf[255]; f++){
            finalData[f] = databuf[f];
        }
//    }
    
    datastate = false;

}


void Mavericks485::dataPut (unsigned char data){
    devSerial.putc(data);
    wait_us(sendtime);
}

void Mavericks485::write (unsigned char *sendData, unsigned char datalength){
    
    REDE = 1;
    
    unsigned char chksum = calc_chksum(sendData, datalength);

    dataPut(0xFA);
    dataPut(0xAF);
    dataPut(datalength);
    for (unsigned char f=0; f<datalength; f++) {
        dataPut(sendData[f]);
    }
    dataPut(chksum);

    REDE = 0;
}

void Mavericks485::write (unsigned char slaveAddress, unsigned char *sendData, unsigned char datalength){
    
    REDE = 1;
    
    unsigned char chksum = calc_chksum(sendData, datalength);

    dataPut(0xFA);
    dataPut(0xAF);
    dataPut(slaveAddress);
    dataPut(datalength);
    if(datalength > 1){
        for(unsigned char f=0; f<datalength; f++){
            dataPut(sendData[f]);
        }
    }else{
        dataPut(sendData[0]);
    }
    dataPut(chksum);

    REDE = 0;
}


bool Mavericks485::readable(){

    return datastate;
}


unsigned int Mavericks485::chkerror(){
  return error;
}

