#ifndef Mavericks485_H_
#define Mavericks485_H_

class Mavericks485{
  public:

    Mavericks485(PinName TXpin, PinName RXpin, PinName REDEpin);
  
    void begin (unsigned char myAddress, bool enable_attach, int baud);
    void read (unsigned char *finalData, unsigned char datalength);
    void readattach (void);
    void write (unsigned char *sendData, unsigned char datalength);
    void write (unsigned char slaveAddress, unsigned char *sendData, unsigned char datalength);
    bool readable();
    unsigned int chkerror ();
   
  protected:

  private:
    mbed::RawSerial devSerial;
    mbed::DigitalOut REDE;
    volatile unsigned char data[256];
    volatile unsigned char databuf[256];
    volatile unsigned char receive;
    volatile unsigned char i;
    unsigned char address;
    int serialbaud;
    int error;
    bool enable;
    volatile bool devMode;
    volatile bool datastate;
    volatile char receive_state;
    volatile unsigned char dataSize;
    float sendtime;
//    mbed::Serial pc;

    
    void sort();
    unsigned char calc_chksum (volatile unsigned char *Data, uint8_t datalength);
    void dataPut(unsigned char data);
    
};

#endif
